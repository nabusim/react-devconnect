const users = require('./users.routes');
const profiles = require('./profiles.routes');
const posts = require('./posts.routes');

// End points
module.exports = app => {
  app.use('/api/users', users);
  app.use('/api/profiles', profiles);
  app.use('/api/posts', posts);
};
