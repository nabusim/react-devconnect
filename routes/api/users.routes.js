const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');

// Load User model
const User = require('../../models/User');
// Load secret key
const keys = require('../../keys');
// Load Input Validation
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require('../../validation/login');

// @route   GET /api/users/test
// @desc    Test users route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Users works' }));

// @route   POST /api/users/register
// @desc    Register users route
// @access  Public
router.post('/register', async (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  try {
    const user = await User.findOne({ email: req.body.email });
    if (user) {
      errors.email = 'Email already exist';
      return res.status(400).json(errors);
    } else {
      const avatar = gravatar.url(req.body.email, {
        s: '200', // Size
        r: 'pg', // Rating
        d: 'mm' // Default
      });
      // instantiate new User
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        avatar,
        password: req.body.password
      });
      const hashedPassword = await hashPassword(newUser);
      try {
        newUser.password = hashedPassword;
        const newuser = await newUser.save();
        res.status(200).json(newuser);
      } catch (error) {
        console.log(error);
      }
    }
  } catch (e) {
    console.log(e);
  }
});

// @route   POST /api/users/login
// @desc    Login user / return JWT token
// @access  Public
router.post('/login', async (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  try {
    const email = req.body.email;
    const password = req.body.password;
    const user = await User.findOne({ email });
    if (!user) {
      errors.email = 'User not found';
      return res.status(404).json(errors);
    }

    // Check password
    const isMatch = await bcrypt.compare(password, user.password);
    if (isMatch) {
      // User matched
      const payload = { id: user.id, name: user.name, avatar: user.avatar }; // JWT Payload
      try {
        const token = await jwtSign(payload, keys.secretOrKey);
        res.json({
          success: true,
          token: 'Bearer ' + token
        });
      } catch (e) {
        console.log(e);
      }
    } else {
      errors.password = 'Password wrong';
      return res.status(400).json(errors);
    }
  } catch (error) {
    console.log(error);
  }
});

// @route   GET /api/users/current
// @desc    return current user
// @access  Private
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json({ id: req.user.id, name: req.user.name, email: req.user.email });
  }
);

// https://stackoverflow.com/questions/48799894/trying-to-hash-a-password-using-bcrypt-inside-an-async-function
async function hashPassword(user) {
  const password = user.password;
  const saltRounds = 10;

  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, function(err, hash) {
      if (err) reject(err);
      resolve(hash);
    });
  });

  return hashedPassword;
}

// https://stackoverflow.com/questions/53051556/how-to-make-callback-function-in-async-await-style
async function jwtSign(payload, secret) {
  return new Promise((resolve, reject) => {
    jwt.sign(payload, secret, { expiresIn: 3600 }, (error, token) => {
      if (error) {
        reject(error);
      } else {
        resolve(token);
      }
    });
  });
}

module.exports = router;
