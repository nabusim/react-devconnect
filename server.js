const express = require('express');
const mongoose = require('mongoose');
const config = require('./keys');
const morgan = require('morgan');
const cors = require('cors');
const passport = require('passport');

const app = express();

// MiddleWares
app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
// Passport middleware
app.use(passport.initialize());
// Passport config
require('./config/passport')(passport);

// Start Server
app.listen(config.port, () => {
  mongoose.connect(config.mongoURI, { useNewUrlParser: true });
});

const db = mongoose.connection;

db.on('error', err => console.log(err));

// Routes
db.once('open', () => {
  require('./routes/api/index.routes')(app);
  console.log(`Server listening on port ${config.port}`);
});
