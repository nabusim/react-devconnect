require('dotenv').config();
module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  url: process.env.BASE_URL,
  secretOrKey: process.env.SECRET,
  mongoURI: process.env.MONGODB_URI
};
